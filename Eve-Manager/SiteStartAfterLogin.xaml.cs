﻿using System;
using System.Diagnostics;
using Windows.Data.Json;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve_Manager
{
    /// <summary>
    /// Startpage for user who are logged in
    /// </summary>
    public sealed partial class SiteStartAfterLogin : Page
    {
        private string accessToken;

        public SiteStartAfterLogin()
        {
            this.InitializeComponent();

            this.accessToken = cStorage.get("access_token");

            createUserImage();
            requestCharacterData();
        }

        private async void createUserImage()
        {

            cRequests characterEndpoints = new cRequests();
            JsonObject response = await characterEndpoints.getCharacterEndpoints();
            Debug.WriteLine(response, "characterEndpoints");

            //Debug.WriteLine(response.GetObject()["portrait"], "portrait");
            BitmapImage image = new BitmapImage(new Uri(response["portrait"].GetObject()["256x256"].GetObject()["href"].GetString()));
            CharacterPortrait.Source = image;

            /*Debug.WriteLine(response["contracts"].GetObject()["href"].GetString(), "contracts");
            cRequests requestCharacterContacts = new cRequests();
            JsonObject contacts = await requestCharacterContacts.GetRequest(response["contracts"].GetObject()["href"].GetString(), cHiddenData.authorizationCrest + accessToken);
            */
        }

        private async void requestCharacterData()
        {
            cRequests authRequest = new cRequests();
            JsonObject contentObject = await authRequest.GetRequest(cHiddenData.UrlAuth + "verify", cHiddenData.authorizationCrest + accessToken);

            Debug.WriteLine(contentObject, "contentObject");
            Debug.WriteLine(contentObject["CharacterID"], "CharacterID");
            Debug.WriteLine(contentObject["CharacterName"], "CharacterName");
            Debug.WriteLine(contentObject["ExpiresOn"], "ExpiresOn");
            Debug.WriteLine(contentObject["Scopes"], "Scopes");
            Debug.WriteLine(contentObject["TokenType"], "TokenType");
            Debug.WriteLine(contentObject["CharacterOwnerHash"], "CharacterOwnerHash");
            Debug.WriteLine(contentObject["IntellectualProperty"], "IntellectualProperty");

            //ClassStorage.save("access_token", contentObject["access_token"].GetString());
            //ClassStorage.save("refresh_token", contentObject["refresh_token"].GetString());

            

        }
    }
}
