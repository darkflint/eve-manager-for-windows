﻿using System.Diagnostics;
using System.Collections.Generic;

namespace Eve_Manager
{
    class cSSOLoginForm
    {
        private static string tmpUri = string.Empty;
        private static string ssoUri = "https://login.eveonline.com/oauth/authorize?response_type=code&redirect_uri=eveauth-evemanager://callback/"
            + "&client_id=" + cHiddenData.clientId
            + "&state=" + cHiddenData.stateLogin
            + "&scope=";

        private static Dictionary<string, string> ssoRights = new Dictionary<string, string>();

        public static void setSSORight(Dictionary<string, string> requestedRights)
        {
            ssoRights = requestedRights;
        }

        public static string ssolink
        {
            get
            {
                tmpUri = ssoUri;
                foreach (KeyValuePair<string, string> ssoRight in ssoRights)
                {
                    tmpUri += ssoRight.Value + "+";
                }
                return tmpUri;
            }
        }
    }
}
