﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Eve_Manager
{
    class cNavigate
    {
        public void to(System.Type siteName)
        {
            Frame frame = new Frame();
            frame.Navigate(siteName);
            Window.Current.Content = frame;

            // Ensure the current window is active
            Window.Current.Activate();
            //this.Frame.Navigate(typeof(SiteStartAfterLogin));
        }
    }
}
