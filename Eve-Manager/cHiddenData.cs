﻿using System;
using System.Collections.Generic;

namespace Eve_Manager
{
    static class cHiddenData
    {
        public const string clientId = "c747dbd51d954493933d79f84fc2be1e";
        public const string secretKey = "4k1HO1ja4nWs9r5AG3YFWYcoSlTZORMa3bqKrj2I";

        private static byte[] authBytes = System.Text.Encoding.UTF8.GetBytes(clientId + ":" + secretKey);
        private static string authBase64 = Convert.ToBase64String(authBytes);
        public static string authorizationAuth = "Basic " + authBase64;
        public static string authorizationCrest = "Bearer ";

        public const string stateLogin = "login";

        public const string UrlAuth = "https://login.eveonline.com/oauth/";
        public const string UrlCrest = "https://crest-tq.eveonline.com/";


        public static Dictionary<string, string> SSORights()
        {
            Dictionary<string, string> ssoRights = new Dictionary<string, string>();
            /* Character */
            // Read
            ssoRights.Add("SSOReadAccount", "characterAccountRead");
            ssoRights.Add("SSOReadAssets", "characterAssetsRead");
            ssoRights.Add("SSOReadBookmarks", "characterBookmarksRead");
            ssoRights.Add("SSOReadCalendar", "characterCalendarRead");
            ssoRights.Add("SSOReadChatChannels", "characterChatChannelsRead");
            ssoRights.Add("SSOReadJumpClones", "characterClonesRead");
            ssoRights.Add("SSOReadContacts", "characterContactsRead");
            ssoRights.Add("SSOReadContracts", "characterContractsRead");
            ssoRights.Add("SSOReadFactionalWarfare", "characterFactionalWarfareRead");
            ssoRights.Add("SSOReadFittings", "characterFittingsRead");
            ssoRights.Add("SSOReadIndustry", "characterIndustryJobsRead");
            ssoRights.Add("SSOReadKills", "characterKillsRead");
            ssoRights.Add("SSOReadLocation", "characterLocationRead");
            ssoRights.Add("SSOReadLoyalPoints", "characterLoyaltyPointsRead");
            ssoRights.Add("SSOReadMails", "characterMailRead");
            ssoRights.Add("SSOReadMarketOrders", "characterMarketOrdersRead");
            ssoRights.Add("SSOReadMedals", "characterMedalsRead");
            ssoRights.Add("SSOReadNotifications", "characterNotificationsRead");
            ssoRights.Add("SSOReadOpportunities", "characterOpportunitiesRead");
            ssoRights.Add("SSOReadResearch", "characterResearchRead");
            ssoRights.Add("SSOReadSkills", "characterSkillsRead");
            ssoRights.Add("SSOReadStats", "characterStatsRead");
            ssoRights.Add("SSOReadWallet", "characterWalletRead");
            // Write
            ssoRights.Add("SSOWriteContracts", "characterContactsWrite");
            ssoRights.Add("SSOWriteAutopilot", "characterNavigationWrite");
            ssoRights.Add("SSOWriteFittings", "characterFittingsWrite");

            /* Corporation */
            // Read
            ssoRights.Add("SSOReadCorpAssets", "corporationAssetsRead");
            ssoRights.Add("SSOReadCorpBookmarks", "corporationBookmarksRead");
            ssoRights.Add("SSOReadCorpContacts", "corporationContactsRead");
            ssoRights.Add("SSOReadCorpContracts", "corporationContractsRead");
            ssoRights.Add("SSOReadCorpFactionalWarfare", "corporationFactionalWarfareRead");
            ssoRights.Add("SSOReadCorpIndustry", "corporationIndustryJobsRead");
            ssoRights.Add("SSOReadCorpKills", "corporationKillsRead");
            ssoRights.Add("SSOReadCorpMarketOrders", "corporationMarketOrdersRead");
            ssoRights.Add("SSOReadCorpMedals", "corporationMedalsRead");
            ssoRights.Add("SSOReadMembers", "corporationMembersRead");
            ssoRights.Add("SSOReadShareholders", "corporationShareholdersRead");
            ssoRights.Add("SSOReadCorpStructures", "corporationStructuresRead");
            ssoRights.Add("SSOReadCorpWallet", "corporationWalletRead");
            // Write
            ssoRights.Add("SSOWriteCorpVulnTimer", "structureVulnUpdate");

            // Fleet
            ssoRights.Add("SSOReadFleet", "fleetRead");
            ssoRights.Add("SSOWriteFleet", "fleetWrite");

            // Public
            ssoRights.Add("SSOReadPublicData", "publicData");
            ssoRights.Add("SSORemoteClientUi", "remoteClientUI");


            return ssoRights;
        }
    }
}
