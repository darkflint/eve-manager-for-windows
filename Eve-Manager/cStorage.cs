﻿using System.Diagnostics;
using Windows.Storage;

namespace Eve_Manager
{
    static class cStorage
    {
        static ApplicationDataContainer localStorage = ApplicationData.Current.LocalSettings;

        public static void save(string key, string value)
        {
            localStorage.Values[key] = value;
        }

        public static string get(string key)
        {
            if (localStorage.Values[key] != null) {
                return localStorage.Values[key].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
