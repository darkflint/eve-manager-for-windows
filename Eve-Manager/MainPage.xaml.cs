﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Diagnostics;
using System.Collections.Generic;
using Windows.Data.Json;
using Windows.Storage;

// Die Vorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 dokumentiert.

namespace Eve_Manager
{
    /// <summary>
    /// StartPage
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Dictionary<string, string> requestedSSORights = new Dictionary<string, string>();

        public MainPage()
        {
            this.InitializeComponent();

            if (cStorage.get("access_token") != string.Empty)
            {
                new cNavigate().to(typeof(SiteStartAfterLogin));
            }
        }

        /// <summary>
        /// test for playing speech
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Click(object sender, RoutedEventArgs e)
        {
            MediaElement mediaElement = new MediaElement();
            var synth = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
            Windows.Media.SpeechSynthesis.SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync("Hello Word");
            mediaElement.SetSource(stream, stream.ContentType);
            mediaElement.Play();
        }

        private void updateSSOUri(object sender, RoutedEventArgs e)
        {
            Dictionary<string, string> getSSORightName = cHiddenData.SSORights();

            CheckBox[] checkboxes = new CheckBox[]
            {
                SSOReadMarketOrders, SSOReadContracts
            };

            foreach (CheckBox checkbox in checkboxes)
            {
                if(checkbox.IsChecked == true && !requestedSSORights.ContainsKey(checkbox.Name))
                {
                    requestedSSORights.Add(checkbox.Name, getSSORightName[checkbox.Name]);
                }
                else if(checkbox.IsChecked == false && requestedSSORights.ContainsKey(checkbox.Name))
                {
                    requestedSSORights.Remove(checkbox.Name);
                }
            }

            cSSOLoginForm.setSSORight(requestedSSORights);
        }

        private void connectSSO(object sender, RoutedEventArgs e)
        {
            if(requestedSSORights.Count > 0)
            {
                new cNavigate().to(typeof(sso));
            }
        }
    }
}
