﻿using System.Diagnostics;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Eve_Manager
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class sso : Page
    {
        private string ssoUri;

        public sso()
        {
            this.InitializeComponent();
            
            this.ssoUri = cSSOLoginForm.ssolink;
            
            webViewSSO.NavigationStarting += webViewSSO_NavigationStarting;
            webViewSSO.NavigationCompleted += webViewSSO_NavigationCompleted;
            webViewSSO.NewWindowRequested += webViewSSO_NewWindowRequested;

        }


        private void webViewSSO_NavigationStarting(object sender, WebViewNavigationStartingEventArgs args)
        {
            //Debug.WriteLine("Start Navigation");
            //Debug.WriteLine(sender, "sender");
            //Debug.WriteLine(webViewSSO, "webViewSSO");
            //Debug.WriteLine(webViewSSO.Source, "webViewSSO source");
            //Debug.WriteLine(webViewSSO.BaseUri, "BaseUri");
            Debug.WriteLine(args.Uri, "Starting uri");
        }

        private void webViewSSO_NavigationCompleted(WebView webview, WebViewNavigationCompletedEventArgs args)
        {
            //Debug.WriteLine(this, "Completed this");
            //Debug.WriteLine(webview.Source, "Completed webview source");
            Debug.WriteLine(args.Uri, "Completed uri");
        }

        private void webViewSSO_NewWindowRequested(WebView webview, WebViewNewWindowRequestedEventArgs args)
        {
            //Debug.WriteLine(this, "requested this");
            //Debug.WriteLine(webview, "requested webview");
            Debug.WriteLine(args.Uri, "requested uri");
        }
    }
}
