﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Web.Http;

namespace Eve_Manager
{
    class cRequests
    {
        /*
         * Refresh Token:
         * requestParams["grant_type"] = "refresh_token"
         * requestParams["refresh_token"] = refreshToken from localStorage
         * */

        public async Task<JsonObject> PostRequest(string url, string authorization, Dictionary<string, string> requestParams, [CallerMemberName] string callerName = "")
        {
            Debug.WriteLine(url, "PostRequest url");

            HttpFormUrlEncodedContent authParams = new HttpFormUrlEncodedContent(requestParams);

            try
            {
                HttpClient authClient = new HttpClient();
                authClient.DefaultRequestHeaders.Add("Authorization", authorization);
                HttpResponseMessage responseMessage = await authClient.PostAsync(new Uri(url), authParams);
                
                authClient.Dispose();

                if (responseMessage.IsSuccessStatusCode)
                {
                    return JsonValue.Parse(await responseMessage.Content.ReadAsStringAsync()).GetObject();
                }
                else
                {
                    if (JsonValue.Parse(responseMessage.Content.ToString()).GetObject()["error"].GetString() == "invalid_token")
                    {

                        if (await refreshAccessToken())
                        {
                            await PostRequest(url, authorization, requestParams, callerName);
                            // only to prevent error message "not all code paths return a value"
                            return new JsonObject();
                        }
                        else
                        {
                            Debug.WriteLine(responseMessage.Content.ToString());
                            return JsonValue.Parse(responseMessage.StatusCode.ToString()).GetObject();
                        }
                    }
                    else
                    {
                        Debug.WriteLine(responseMessage.Content.ToString());
                        return JsonValue.Parse(responseMessage.StatusCode.ToString()).GetObject();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Fail(e.Message.ToString());
                return JsonValue.Parse(e.Message.ToString()).GetObject();
            }
        }

        public async Task<JsonObject> GetRequest(string url, string authorization = null, [CallerMemberName] string callerName = "")
        {
            Debug.WriteLine(url, "GetRequest url");

            try
            {
                HttpClient authClient = new HttpClient();

                if(authorization != null)
                {
                    authClient.DefaultRequestHeaders.Add("Authorization", authorization);
                }
                
                HttpResponseMessage responseMessage = await authClient.GetAsync(new Uri(url));

                authClient.Dispose();

                if (responseMessage.IsSuccessStatusCode)
                {
                    return JsonValue.Parse(await responseMessage.Content.ReadAsStringAsync()).GetObject();
                }
                else
                {
                    if (JsonValue.Parse(responseMessage.Content.ToString()).GetObject()["error"].GetString() == "invalid_token")
                    {

                        if (await refreshAccessToken())
                        {
                            await GetRequest(url, authorization, callerName);
                            // only to prevent error message "not all code paths return a value"
                            return new JsonObject();
                        }
                        else
                        {
                            Debug.WriteLine(responseMessage.Content.ToString());
                            return JsonValue.Parse(responseMessage.StatusCode.ToString()).GetObject();
                        }
                    }
                    else
                    {
                        Debug.WriteLine(responseMessage.Content.ToString());
                        return JsonValue.Parse(responseMessage.StatusCode.ToString()).GetObject();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Data.ToString(), "data");
                Debug.WriteLine(e.HResult.ToString(), "HResult");
                Debug.WriteLine(e.InnerException.ToString(), "innerException");
                Debug.WriteLine(e.Source.ToString(), "source");
                Debug.WriteLine(e.StackTrace.ToString(), "stacktrace");
                Debug.Fail(e.Message.ToString(), "sso exception");
                return JsonValue.Parse(e.Message.ToString()).GetObject();
            }
        }

        public async Task<JsonObject> getEndpoints(string category = null)
        {
            cRequests getEndponts = new cRequests();
            JsonObject endpoints = await getEndponts.GetRequest(cHiddenData.UrlCrest);

            Debug.WriteLine(endpoints, "endpoints");

            if(category != null)
            {
                endpoints = endpoints[category].GetObject();
            }

            return endpoints;
        }

        public async Task<JsonObject> getCharacterEndpoints()
        {
            JsonObject endpoints = await getEndpoints("decode");

            cRequests getDecoder = new cRequests();
            JsonObject decoder = await getDecoder.GetRequest(endpoints["href"].GetString(), cHiddenData.authorizationCrest + cStorage.get("access_token"));
            
            cRequests getCharacterData = new cRequests();
            JsonObject characterEndpoints = await getCharacterData.GetRequest(decoder["character"].GetObject()["href"].GetString(), cHiddenData.authorizationCrest + cStorage.get("access_token"));

            return characterEndpoints;
        }

        private async Task<Boolean> refreshAccessToken()
        {
            cRequests requestNewAccessToken = new cRequests();

            Dictionary<string, string> requestParams = new Dictionary<string, string>();
            requestParams.Add("grant_type", "refresh_token");
            requestParams.Add("refresh_token", cStorage.get("refresh_token"));

            JsonObject refreshToken = await requestNewAccessToken.PostRequest(cHiddenData.UrlAuth + "token", cHiddenData.authorizationAuth, requestParams);

            cStorage.save("access_token", refreshToken.GetObject()["access_token"].GetString());

            //ToDo: fail check
            return true;
        }
    }
}